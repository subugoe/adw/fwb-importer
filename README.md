# Data importer for FWB-online (Frühneuhochdeutsches Wörterbuch online)

## Description

The project's main purpose is to convert specifically formatted TEI files to Solr XML files. It also
contains plugin classes which assert that the results are correct. Furthermore, it contains the
complete configuration which is needed for a Solr server (schema, solrconfig.xml, etc.). In
combination with the solr-importer, it becomes a web user interface (UI) tool that can be used to
convert and import files delivered by the FWB-online project into the Solr server of its web site
(https://fwb-online.de).

## Host system requirements

Docker, docker-compose, Git.

## Deployment / CICD-Pipeline

For the deployment of the fwb-importer to dev, staging, stagingall (contains unpublishable data) and
live, the docker images are used. They are build with the Gitlab-cicd-pipeline.
There are 3 different images, distinguished by their tag: `develop`, `cleanup_new_tei` (used for
staging and live) and `cleanup_new_tei_all` (for stagingall).
Develop is build from the develop branch, the live and staging images should be build from master
branch.
The docker-compose.yml file shows an example of how to use the fwb-importer, it is not used for
the deployment on the VMs.

## Project Structure

This project consists of two sub-projects: web and fwb-importer-core. Web contains the main-class
and is a Thymeleaf application. It is the front-end and offers the possibility to control the import
via a website. Fwb-importer-core is used there as a library and runs the data conversion and finally
the import into Solr. Please consider the file fwb-importer-core/src/main/resources/context.xml. In
this file the steps of the importer are assembled. This file is read from the web-project through
the class fwb-importer-core/src/main/java/sub/ent/backend/BeanRetriever.java.

## Restructuring the project in 2024

The project layout before the reorganisation was different. There were 2 repositories, this one on
Gitlab and https://github.com/subugoe/solr-importer. The aim was to offer a generic solution for the
importer that could be reused in other projects. However, this reuse was actually not used, which
is why the complexity of this project (fwb-importer) was unnecessarily high.
With the reorganisation, the code from the Github repository was integrated into this repository.
Previously there were basically three (sub-)projects: core, web and fwb-plugin. Core and web were in
the Github project, (the solr-importer). In the new layout, core and fwb-plugin are merged in the
folder fwb-importer-core. The web folder was transferred nearly unchanged from the Github project.
The gradle build files have been adapted accordingly

## Starting the importer locally

The Web UI can be started by executing:

```./3-start-importer.sh```

On the first run, the Git repository that contains all FWB TEI files will be cloned (you configured
the repository in the docker.env file). This happens in the background and can last several minutes.
After that, the Web UI should be available on localhost on the (default). Note that this script
doesn't start the solar which must be configured and started seperately.

## System requirements

Linux, Java 8, Gradle 6.8 port 9090.

## Further Documentation

Here in this readme, you can read how to operate the project. For more information on the code,
refer to fwb-importer-core/src/main/java/docs/FwbPluginDocumentation.java and
solr-importer/web/src/main/java/docs/WebModuleDocumentation.java. Please not that this documentation
was written before the restructuration and might refer in parts to the setup before the
restructuration, as does the old wiki: https://github.com/subugoe/solr-importer/wiki.

## Starting in Eclipse

During development, it is very convenient to start the Web UI from inside the IDE 'on-the-fly'
without the need to compile it on the command line. In Eclipse you can easily do this by executing
the class 'WebApplication.java' in the 'web' module via the 'Run As' dialog. The Project has to
imported as a Gradle Project. With this Eclipse should be able to layout the project properly. Also,
since the Web UI needs several environment variables, you must also set them in the 'Run
Configurations' dialog. Take a look at the file 'environment.sh.dist' for details. To finally test
the application a running solr is needed and the environment variables must be set accordingly.

## docker-compose.yml

Apparently the docker-compose file was introduced some year after the project was created. To use
it, the file docker.env is needed. The template docker.env.dist can be used as a starting point.
Additionally GIT_URL must be set inside the docker-compose.yml or inside of `.env`, because it is
used as a build argument and thus setting it in docker.env will not work. For a minimal running
version at least the variables SOLR_STAGING_URL, SOLR_LIVE_URL, GIT_URL, GIT_USER and GIT_PASSWORD
must be set. Note that the repo name (the last part of the url) of GIT_URL must fit to the folder
name of GIT_INPUT_DIR inside the docker-compose. Example: GIT_URL:
https://github.com/joschrew/fwb-test-daten.git, GIT_INPUT_DIR=/git/fwb-test-daten.
This is because inside the Dockerfile of the importer the repo is cloned without destination folder
name into the dir /git and then accessd through GIT_INPUT_DIR within the java code.
Maybe the docker-compose.yml should be removed again because it is not needed for deployment and it
is cumbersome using it when developing.
