package sub.fwb;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.PutObjectRequest;

import java.io.File;

public class S3Uploader {
    private AmazonS3 s3Client;
    
    public S3Uploader(String accessKey, String secretKey, String endpoint, boolean useHttps) {
        // Set up AWS credentials
        BasicAWSCredentials awsCredentials = new BasicAWSCredentials(accessKey, secretKey);

        // Endpoint configuration with HTTP or HTTPS
        AwsClientBuilder.EndpointConfiguration endpointConfig = new AwsClientBuilder.EndpointConfiguration(
                endpoint, "");

        // Create an S3 client with ForcePathStyle enabled
        s3Client = AmazonS3ClientBuilder.standard()
                .withEndpointConfiguration(endpointConfig)
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .withPathStyleAccessEnabled(true) // ForcePathStyle equivalent
                .withClientConfiguration(new com.amazonaws.ClientConfiguration().withProtocol(
                    useHttps ? com.amazonaws.Protocol.HTTPS : com.amazonaws.Protocol.HTTP))
                .build();
    }
    
    public void uploadFile(File file, String bucketName, String keyName) {
        // Create a PutObjectRequest
        PutObjectRequest request = new PutObjectRequest(bucketName, keyName, file);

        // Upload the file
        s3Client.putObject(request);

        System.out.println("File uploaded successfully to S3.");
    }

}
