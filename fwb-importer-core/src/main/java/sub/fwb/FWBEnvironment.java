package sub.fwb;

/**
 * Access to environment variables.
 */
public class FWBEnvironment {

    public final String UNDEFINED_VALUE = "undefined";

    /**
     * Gets an environment variable by name.
     */
    private String getVariable(String name) {
        String variable = System.getenv(name);
        if (variable == null) {
            System.err.println("WARNING Environment variable not set: " + name + ". Setting to '" + UNDEFINED_VALUE + "'");
            return UNDEFINED_VALUE;
        }
        return variable;
    }

    public String cacheUrl() {
        return getVariable("FWB_CACHE_URL");
    }

    public String fwbUser() {
        return getVariable("FWB_USER");
    }

    public String fwbPassword() {
        return getVariable("FWB_PASSWORD");
    }

    public String accessKey() {
        return getVariable("STORAGE_KEY"); // New method for access key
    }

    public String secretKey() {
        return getVariable("STORAGE_SECRET"); // New method for secret key
    }

    public String bucketName() {
        return getVariable("S3_BUCKET"); // New method for bucket name
    }

    public String endPoint() {
        return getVariable("STORAGE_ENDPOINT"); // New method for bucket name
    }
}
