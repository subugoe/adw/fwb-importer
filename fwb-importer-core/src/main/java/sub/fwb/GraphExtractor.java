package sub.fwb;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.XMLConstants;
import org.apache.commons.collections.map.MultiKeyMap;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

class GraphNode {
    String id;
    String label;
    String wortart;
    String color;

    GraphNode(String id, String label, String wortart, String color) {
        this.id = id;
        this.label = label;
        this.wortart = wortart;
        this.color = color;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        GraphNode otherNode = (GraphNode) obj;

        return id.equals(otherNode.id) &&
               label.equals(otherNode.label) &&
               wortart.equals(otherNode.wortart);
              //  &&
              // color.equals(otherNode.color);
    }
}

class GraphLink {
    String source;
    String target;
    String art;

    GraphLink(String source, String target, String art) {
        this.source = source;
        this.target = target;
        this.art = art;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        GraphLink otherLink = (GraphLink) obj;

        return source.equals(otherLink.source) &&
               target.equals(otherLink.target) &&
               art.equals(otherLink.art);
    }
}

class Graph {
    List<GraphLink> links;
    List<GraphNode> nodes;

    Graph(List<GraphNode> nodes, List<GraphLink> links) {
        this.nodes = nodes;
        this.links = links;
    }
}

class DirectedGraph {
    private Map<String, Set<GraphNode>> sourceIdToTargetList;
    private Map<String, Set<GraphNode>> targetIdToSourceList;
    private Map<String, GraphNode> nodeMap;
    private MultiKeyMap linksInfos;

    DirectedGraph(List<GraphNode> nodes, List<GraphLink> links) {
        this.nodeMap = new HashMap<>();
        this.linksInfos = new MultiKeyMap();
        this.sourceIdToTargetList = new HashMap<>();
        this.targetIdToSourceList = new HashMap<>();

        for (GraphNode node : nodes) {
            nodeMap.put(node.id, node);
        }

        GraphNode targetNode, sourceNode;

        for (GraphLink link : links) {
            // Source -> [Targets]
            if (!this.sourceIdToTargetList.containsKey(link.source)) {
                sourceIdToTargetList.put(link.source, new HashSet<>());
            }
            targetNode = nodeMap.get(link.target);
            if (targetNode != null) {
                sourceIdToTargetList.get(link.source).add(targetNode);
            }

            // Target -> [Sources)
            if (!this.targetIdToSourceList.containsKey(link.target)) {
                targetIdToSourceList.put(link.target, new HashSet<>());
            }
            sourceNode = nodeMap.get(link.source);
            if (sourceNode != null) {
                targetIdToSourceList.get(link.target).add(sourceNode);
            }

            // Store links `art` information
            linksInfos.put(link.source, link.target, link.art);
        }
    }

    private Set<GraphNode> getSurroundingNodes(String nodeId) {
        Set<GraphNode> targets = sourceIdToTargetList.get(nodeId);
        Set<GraphNode> sources = targetIdToSourceList.get(nodeId);
        if (targets != null) {
        	if (sources != null) {
        		targets.addAll(sources);
        	}
        	return targets;
        } else {
        	if (sources != null) {
        		return sources;
        	}
        	return new HashSet<>();
        }
    }

    private Set<GraphLink> getIntraLinks(Set<GraphNode> nodes) {
        Set<GraphLink> intraLinks = new HashSet<>();
        for (GraphNode srcNode : nodes) {
            for (GraphNode destNode : nodes) {
                 Object art = linksInfos.get(srcNode.id, destNode.id);
                 if (art != null) {
                     intraLinks.add(new GraphLink(srcNode.id, destNode.id, art.toString()));
                 }

            }
        }
        return intraLinks;
    }

    Graph getNetworkWithDepth(String nodeId, int depth) {
        Set<GraphNode> nodes = new HashSet<>(); // Use a Set to avoid duplicates
        Set<GraphLink> links;
        List<GraphNode> currentNodes = new ArrayList<>();
        Set<GraphNode> visitedNodes = new HashSet<>(); // Track visited nodes

        GraphNode startNode = nodeMap.get(nodeId);
        if (startNode == null) {
            System.out.println("Node with ID " + nodeId + " does not exist.");
            return new Graph(new ArrayList<>(), new ArrayList<>());
        }
        nodes.add(startNode);
        currentNodes.add(startNode);
        visitedNodes.add(startNode);

        for (int i = 0; i < depth; i++) {
            List<GraphNode> nextNodes = new ArrayList<>();
            for (GraphNode currentNode : currentNodes) {
                Set<GraphNode> surroundingNodes = getSurroundingNodes(currentNode.id);
                for (GraphNode surroundingNode : surroundingNodes) {
                    if (!visitedNodes.contains(surroundingNode)) {
                        visitedNodes.add(surroundingNode);
                        nextNodes.add(surroundingNode);
                    }
                }
            }
            nodes.addAll(nextNodes);
            currentNodes = nextNodes;
        }

        links = getIntraLinks(nodes);
        return new Graph(new ArrayList<>(nodes), new ArrayList<>(links));
    }
}


public class GraphExtractor {

    private DirectedGraph mainGraph;
    //constructor

    public GraphExtractor(String path) {
        mainGraph = readGraphFromFile(path);
    }

    public String getJSONs(String xmlFilePath) {
        Map<String, JsonObject> jsons = new HashMap<>();
        ArrayList<String> meaningsList = getMeaningsList(xmlFilePath);

        Gson gson = new Gson(); // Standard Gson without pretty printing

        for (String meaning : meaningsList) {
            for (int depth = 1; depth <= 3; depth++) {  // Adjust depth as needed
                Graph subGraph = mainGraph.getNetworkWithDepth(meaning, depth);

                // Serialize subGraph as a JsonObject, not String, to avoid unnecessary escapes
                JsonObject jsonResult = gson.toJsonTree(subGraph).getAsJsonObject();
                jsons.put(meaning + "_depth_" + depth, jsonResult);
            }
        }

        // Serialize the entire map to JSON in a compact format
        return gson.toJson(jsons);
    }

    private static ArrayList<String> getMeaningsList(String xmlFilePath) {
        ArrayList<String> idValues = new ArrayList<>();

        List<String> lines = new ArrayList<>();
        try {
            // Read the XML file line by line
            try (BufferedReader reader = new BufferedReader(new FileReader(xmlFilePath))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    lines.add(line);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("Error reading meanings from tei-File", e);
        }


        // Combine lines into a single string
        String xmlContent = String.join("", lines);

        // Use this regex to find multi sense lemmas
        Pattern pattern = Pattern.compile("<sense\\s+[^>]*xml:id=\"([^\"]+)\"[^>]*>");
        Matcher matcher = pattern.matcher(xmlContent);
        
        while (matcher.find()) {
            String idValue = matcher.group(1);
            idValues.add(idValue);
        }

        // Single sense lemmas have the id in the entry element
        if (idValues.isEmpty()) {
            Pattern pattern2 = Pattern.compile("<entry\\s+[^>]*xml:id=\"([^\"]+)\"[^>]*>");

            // Use regex to read the lemma's id from the entry element in case it has only one meaning
            matcher = pattern2.matcher(xmlContent);

            // Extract and add the id value to the list
            while (matcher.find()) {
                String idValue = matcher.group(1);
                idValues.add(idValue);
            }
        }

        return idValues;
    }

    public static Graph parseGraphFromJson(String jsonString) {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            JsonNode root = objectMapper.readTree(jsonString);
            List<GraphNode> nodes = new ArrayList<>();
            List<GraphLink> links = new ArrayList<>();

            // Parse Nodes
            for (JsonNode node : root.get("nodes")) {
                String id = node.get("id").asText();
                String label = node.get("label").asText();
                String wortart = node.get("wortart").asText();
                String color = node.get("color").asText();

                nodes.add(new GraphNode(id, label, wortart, color));
            }

            // Parse Links
            for (JsonNode link : root.get("links")) {
                String source = link.get("source").asText();
                String target = link.get("target").asText();
                String art = link.get("art").asText();

                links.add(new GraphLink(source, target, art));
            }

            return new Graph(nodes, links);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static DirectedGraph readGraphFromFile(String filePath) {
        List<GraphNode> nodes = new ArrayList<>();
        List<GraphLink> links = new ArrayList<>();

        try {
            SAXBuilder sax = new SAXBuilder();
            sax.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
            sax.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");

            org.jdom2.Document document = sax.build(new File(filePath));

            Element rootElement = document.getRootElement();
            System.out.println(rootElement.getName());

            Element graphElement = rootElement.getChildren().get(0);
            System.out.println(graphElement.getName());

            Element node_child = graphElement.getChildren().get(2);
            System.out.println(node_child.getName());

            Element edge_child = graphElement.getChildren().get(3);
            System.out.println(edge_child.getName());

            List<Element> nodesElements = node_child.getChildren();
                // Process nodes
                for (Element nodeElement : nodesElements) {

                    String id = nodeElement.getAttributeValue("id");
                    String label = nodeElement.getAttributeValue("label");
                    String wortart = nodeElement.getChildren()
                            .get(0).getChildren().get(0)
                            .getAttributeValue("value");
                    // Get color element
                    Element colorElement = nodeElement.getChildren().get(1);

                    // Extract color components
                    int red = Integer.parseInt(colorElement.getAttributeValue("r"));
                    int green = Integer.parseInt(colorElement.getAttributeValue("g"));
                    int blue = Integer.parseInt(colorElement.getAttributeValue("b"));

                    // Create color string in hexadecimal format
                    String color = String.format("#%02X%02X%02X", red, green, blue);

                    nodes.add(new GraphNode(id, label, wortart, color));

                }
                List<Element> edgesElements = edge_child.getChildren();
                // Process edges/links
                for (Element edgeElement : edgesElements) {
                    String source = edgeElement.getAttributeValue("source");
                    String target = edgeElement.getAttributeValue("target");
                    String art = edgeElement.getChildren().get(0).getChildren().get(0)
                            .getAttributeValue("value");

                    links.add(new GraphLink(source, target, art));
                }

        } catch (JDOMException | IOException e) {
            System.out.println("Error while parsing XML file!");
            e.printStackTrace();
        }

        return new DirectedGraph(nodes, links);
    }


}
