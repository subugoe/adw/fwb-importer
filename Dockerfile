FROM gradle:6.8 AS build
ARG GIT_URL
ARG WORKING_DIRECTORY=/project

WORKDIR $WORKING_DIRECTORY

COPY . $WORKING_DIRECTORY
RUN gradle --gradle-user-home $WORKING_DIRECTORY/.gradle-user-home

FROM eclipse-temurin:8
ARG GIT_URL
ARG WORKING_DIRECTORY=/project

RUN apt-get -yqq update && apt-get install -y git && \
    mkdir /git && \
    cd /git && \
    git clone $GIT_URL

COPY --from=build $WORKING_DIRECTORY/web/build/libs/web-0.0.1-SNAPSHOT.jar /tmp/web-importer.jar

CMD ["java", "-jar", "/tmp/web-importer.jar"]

VOLUME /git
VOLUME /import-files
